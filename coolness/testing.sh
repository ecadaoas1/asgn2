#!/bin/bash -v

# Test case with normal inputs
./coolness 20 10 

# Test case with no arguments
./coolness

# Test case with invalid temperature value
./coolness 55 10 2

# Test case with invalid wind speed value
./coolness 20 0.3 2

make clean
