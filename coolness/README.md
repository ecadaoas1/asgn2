PA2 Part 1: Coolness Calculator

This is a program that calculates the coolness factor based on temperature and wind speed.

1. Compile

    gcc -o coolness coolness.c -lm

2. Run program

- Calculate coolness for range of parameters
    
    ./coolness

- Calculate coolness given temperature
    
    ./coolness [temperature]

- Calculate coolness given temperature and wind speed
    
    ./coolness [temperature] [wind speed]

The program accepts zero, one, or two command-line arguments

    - If no arguments are provided, coolness is calculated for a range of temperature and wind speed values.
    - If one argument is provided, coolness is calculated for a specified temperature and a range of wind speeds.
    - If two arguments are provided, coolness is calculated for specified temperature and wind speed values.
    

The program also validates the input values and prints the temperature, wind speed, and coolness values.

