#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Function to calculate
double calculateCoolness(double T, double V) {
    return 35.74 + 0.6215 * T - 35.75 * pow(V, 0.16) + 0.4275 * T * pow(V, 0.16);
}

int main(int argc, char *argv[]) {
    if (argc == 1) {
        // Range of parameters
        for (double T = -10; T <= 40; T += 10) {
            for (double V = 5; V <= 15; V += 5) {
                double coolness = calculateCoolness(T, V);
                printf("Coolness for T=%.2f and V=%.2f: %.2f\n", T, V, coolness);
            }
        }
    } else if (argc == 2 || argc == 3) {
        // Parse temperature argument
        double T = atof(argv[1]);

        if (T < -99.0 || T > 50.0) {
            fprintf(stderr, "Error: Coolness. Acceptable input values are -99<=T<=50 and 0.5<=V.\n");
            return 1;
        }

        if (argc == 2) {
            // Print coolness for various wind speeds
            for (double V = 5; V <= 15; V += 5) {
                double coolness = calculateCoolness(T, V);
                printf("Coolness for T=%.2f and V=%.2f: %.2f\n", T, V, coolness);
            }
        } else {
            // Parse wind speed argument
            double V = atof(argv[2]);

            if (V < 0.5) {
                fprintf(stderr, "Error: Coolness. Acceptable input values are -99<=T<=50 and 0.5<=V.\n");
                return 1;
            }

            // Given temperature and wind speed
            double coolness = calculateCoolness(T, V);
            printf("Coolness for T=%.2f and V=%.2f: %.2f\n", T, V, coolness);
        }
    } else {
        // Incorrect number of arguments
        fprintf(stderr, "Usage: %s [temperature] [wind speed]\n", argv[0]);
        return 1;
    }

    return 0;
}
