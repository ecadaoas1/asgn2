PA2 Part : Histogram

This program generates a histogram for a set of values. It uses a fixed number of bins and resizes them according to the input range. The code reads integer values from the input and creates a histogram.

1. Compile
   
    gcc -o histogram histogram.c

2. Run program
    
    ./histogram

3. Enter interger values in each line and enter Ctrl+D to finish input.

4. The program generates a histogram and prints it in the console.

The program reads integers from the input and for each value:

    - resizes bins and adjusts range if the value exceeds the current histogram range.
    - stores value in an array if it is positive
    - places values into correct bins
