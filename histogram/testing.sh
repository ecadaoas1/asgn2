#!/bin/bash -v

# Function for running test case
run_test() {
    echo "Input: $1"
    echo $1 | ./histogram.c
    echo
}

# Compile
make

# Test cases
run_test "1 2 3 4 5 6 7 8 9 10"
run_test "1 2 3 4 5 6 7 8 9 10 15 20 30 40 60 70"
run_test "1 -2 3 4 -5 6 7 8 9 10"
run_test "1000 2000 4000"

# Clean up
make clean
