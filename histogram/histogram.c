#include <stdio.h>
#include <stdlib.h>

#define NUM_BINS 16

// Print histogram function
void printHistogram(int bins[NUM_BINS], int binSize, int rangeStart) {
    for (int i = 0; i < NUM_BINS; i++) {
        int rangeEnd = rangeStart + binSize;
        printf("[%3d:%3d] ", rangeStart, rangeEnd - 1);
        for (int j = 0; j < bins[i]; j++) {
            printf("*");
        }
        printf("\n");
        rangeStart = rangeEnd;
    }
}

int main() {
    int bins[NUM_BINS] = {0};  // Default bins to 0
    int binSize = 1;
    int rangeStart = 0;

    printf("%d bins of size %d for range [%d,%d)\n", NUM_BINS, binSize, rangeStart, rangeStart + (binSize * NUM_BINS));

    int value;
    while (scanf("%d", &value) != EOF) {
        if (value < 0) {
            // Ignore negative integers
            continue;
        }

        // Check if the value is within range
        while (value < rangeStart || value >= rangeStart + (binSize * NUM_BINS)) {
            // Update bin size
            binSize *= 2;
            rangeStart = 0;

            // Compress the bins
            for (int i = NUM_BINS - 1; i >= 8; i--) {
                bins[i] = bins[i - 8] + bins[i];
                bins[i - 8] = 0;
            }

            printf("%d bins of size %d for range [%d,%d)\n", NUM_BINS, binSize, rangeStart, rangeStart + (binSize * NUM_BINS));
        }

        // Update the correct bin
        int binIndex = (value - rangeStart) / binSize;
        bins[binIndex]++;
    }

    // Print histogram
    printHistogram(bins, binSize, rangeStart);

    return 0;
}
